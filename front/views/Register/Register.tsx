import React from 'react';
import loginStyles from '@styles/Login.module.css';
import { Input, Button, Form } from 'antd';
import Link from 'next/link';
interface OwnProps {}

type Props = OwnProps;

const Register: React.FC<Props> = (props: Props) => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };
  return (
    <div className={loginStyles.root}>
      <Form name="basic" onFinish={onFinish}>
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>
        <div className={loginStyles.loginButtonWrapper}>
          <Button shape="round" type="primary" htmlType="submit">
            Login
          </Button>
          <Link href="/register">
            <Button shape="round" type="primary" className={loginStyles.registerWrapper}>
              Register
            </Button>
          </Link>
        </div>
      </Form>
    </div>
  );
};

export default Register;
