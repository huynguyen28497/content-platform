import React from 'react';
import { useSelector } from 'react-redux';
import { StoreState } from '@stores';
import { UserState } from '@reducers/user.reducer';

interface OwnProps {}

type Props = OwnProps;

const Home: React.FC<Props> = (props: Props) => {
  const user = useSelector<StoreState, UserState>((state) => state.user);
  return (
    <div>
      <h1>{user.name}</h1>
    </div>
  );
};

export default Home;
