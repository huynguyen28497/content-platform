import reducer from '@reducers';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import apiMiddleware from '../middlewares/api.middleware';

const logger = createLogger();

const store = createStore(reducer, compose(applyMiddleware(thunk, apiMiddleware, logger)));
export default store;

export type StoreState = ReturnType<typeof reducer>;
