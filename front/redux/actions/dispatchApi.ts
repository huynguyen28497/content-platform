import { CALL_API } from '../middlewares/api.middleware';
import { Action } from 'redux';
import { Method, AxiosRequestConfig } from 'axios';

interface ICallApiInfo {
  types: string[];
  endpoint: string;
  method: Method;
  body: AxiosRequestConfig;
}

export interface IApiAction extends Action {
  [CALL_API]: ICallApiInfo;
}

const dispatchApi = <T>(dispatch: any, { types, endpoint, method, body }: ICallApiInfo) =>
  dispatch({
    type: '',
    [CALL_API]: {
      types,
      endpoint,
      method,
      body,
    },
  }) as Promise<T>;

export default dispatchApi;
